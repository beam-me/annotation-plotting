# -*- coding:utf-8 -*-
from __future__ import print_function

import os
import re
import sys
import math
import logging
import gdxcc as gdx
import numpy as np
import scipy.sparse as sparse
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.ticker as ticker
import matplotlib.patches as patches
from timeit import default_timer as timer


class GDXError(LookupError):
    """raise this when there's a GAMS GDX API error """


class HiddenPrints:
    def __enter__(self):
        self._original_stdout = sys.stdout
        sys.stdout = None

    def __exit__(self, exc_type, exc_val, exc_tb):
        sys.stdout = self._original_stdout


class oAnnotGDX(object):
    """
    Object that reads gdx file and provides sparse Matrix, information about
    block structure and allows plotting and printing
    """

    @property
    def _sparseA(self):
        if self.__localA is None:
            self.__read_data()
        return self.__localA

    @property
    def _varStage(self):
        if self.__varStage is None:
            self.__read_data()
        return self.__varStage

    @property
    def _varNames(self):
        if self.__varNames is None:
            self.__read_data()
        return self.__varNames

    @property
    def _equStage(self):
        if self.__equStage is None:
            self.__read_data()
        return self.__equStage

    @property
    def _equNames(self):
        if self.__equNames is None:
            self.__read_data()
        return self.__equNames

    @property
    def nbVars(self):
        if self._nbVars is None:
            self._get_block_stats()
        return self._nbVars

    @property
    def nbConstr(self):
        if self._nbConstr is None:
            self._get_block_stats()
        return self._nbConstr

    @property
    def nbNonZero(self):
        if self._nbNonZero is None:
            self._get_block_stats()
        return self._nbNonZero

    @property
    def nbBlocks(self):
        if self._nbBlocks is None:
            self._get_block_stats()
        return self._nbBlocks

    @property
    def nbLinkVars(self):
        if self._nbLinkVars is None:
            self._get_block_stats()
        return self._nbLinkVars

    @property
    def nbLinkCons(self):
        if self._nbLinkCons is None:
            self._get_block_stats()
        return self._nbLinkCons

    @property
    def nbLinkConsA0(self):
        if self._nbLinkConsA0 is None:
            self._get_block_stats()
        return self._nbLinkConsA0

    @property
    def nbLinkConsAn(self):
        if self._nbLinkConsAn is None:
            self._get_block_stats()
        return self._nbLinkConsAn

    @property
    def nbLinkConsAn_local(self):
        if self._nbLinkConsAn_local is None:
            self._get_local_constr()
        return self._nbLinkConsAn_local

    @property
    def nbLinkVars_local(self):
        if self._nbLinkVars_local is None:
            self._get_local_vars()
        return self._nbLinkVars_local

    @property
    def blockStages(self):
        if self._blockStages is None:
            self._get_block_stats()
        return self._blockStages

    @property
    def varStages(self):
        if self._varStages is None:
            self._get_block_stats()
        return self._varStages

    @property
    def linkVarStage(self):
        if self._linkVarStage is None:
            self._get_block_stats()
        return self._linkVarStage

    @property
    def equStages(self):
        if self._equStages is None:
            self._get_block_stats()
        return self._equStages

    @property
    def linkEquStages(self):
        if self._linkEquStages is None:
            self._get_block_stats()
        return self._linkEquStages

    # Default colormap and grey scale flag for plotting and printing
    __colormap = 'viridis'
    __greyscale = False

    def __init__(self, gdx_file, gams_dir, load_numpy=False, save_numpy=False, skip_names=False, log_level='WARNING',
                 plain=False):

        # Get the user specified input
        self.gdxFile = gdx_file
        self.__file_path = None
        self.__file_name = None
        self.__file_extension = None
        self.gams_dir = gams_dir
        self.gdx_path = None
        self.npz_path = None
        self.logger = None
        self.log_level = log_level

        self.__plain = plain
        self.__load_numpy = load_numpy
        self.__save_numpy = save_numpy
        self.__skip_names = skip_names

        # Matrix information provided by function __readData
        self.__localA = None
        self.__varStage = None
        self.__varNames = None
        self.__equStage = None
        self.__equNames = None

        # Listing for dense column checking
        self.__colNNZ = None

        # Evaluated matrix information provided by function
        self._nbVars = None
        self._nbConstr = None
        self._nbNonZero = None

        self._nbBlocks = None
        self._nbLinkVars = None
        self._nbLinkVars_local = None
        self._nbLinkCons = None
        self._nbLinkConsA0 = None
        self._nbLinkConsAn = None
        self._nbLinkConsAn_local = None

        self._blockStages = None
        self._varStages = None
        self._equStages = None
        self._linkVarStage = None
        self._linkEquStages = None

        # Perform version and path checks
        self.__create_logger()
        self.__check_version()
        self.__check_paths()

    def __create_logger(self):
        # create logger
        self.logger = logging.getLogger('annotation_plotting')

        self.logger.setLevel(logging.INFO)

        if self.log_level.lower() == 'debug':
            self.logger.setLevel(logging.DEBUG)

        # create logger with debug level
        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        ch.setFormatter(formatter)
        self.logger.addHandler(ch)

    def __check_version(self):
        if self.__load_numpy and sys.version_info < (3, 4):
            self.logger.warning('Reading from npz not supported for Python versions < 3.4, switching to gdx reading.')
            self.__load_numpy = False

        if self.__save_numpy and sys.version_info < (3, 4):
            self.logger.warning('Numpy cannot save arrays for Python versions < 3.4, disabling saving to npz file.')
            self.__save_numpy = False

    def __check_paths(self):
        # Check GAMS directory
        if not os.path.exists(self.gams_dir):
            raise IOError('GAMS directory not found at: ' + self.gams_dir)

        # Check if file is a gdx (standard assumption)
        self.__file_path, file_ext = os.path.split(self.gdxFile)
        self.__file_name, self.__file_extension = os.path.splitext(file_ext)
        if self.__file_extension == '.gdx':
            self.gdx_path = os.path.join(self.__file_path, self.__file_name) + self.__file_extension
            self.npz_path = os.path.join(self.__file_path, self.__file_name) + '.npz'

            if not os.path.exists(self.gdx_path):
                raise IOError('Input file not found at: ' + self.gdx_path)

            if self.__load_numpy and not os.path.exists(self.npz_path):
                self.logger.warning('No .npz file found at: ' + self.npz_path + '. Switching to gdx reading')
                self.__load_numpy = False

            # Check if gdx is a novenames gdx file (equNames and varNames are omitted in gdx File)
            if "novenames" in self.gdx_path.lower():
                self.__skip_names = True

        # Check if input file is directly given as npz
        elif self.__file_extension == '.npz':
            self.gdx_path = ''
            self.npz_path = os.path.join(self.__file_path, self.__file_name) + self.__file_extension
            if not os.path.exists(self.npz_path):
                raise IOError('Input file not found at: ' + self.npz_path)

            # Overwrite flag to read from numpy file
            self.__load_numpy = True

        else:
            raise IOError('No valid file extension found for: ' + self.gdxFile)

    def __read_data(self):
        # Load data from npz or gdx file
        if self.__load_numpy:
            self.__read_numpy()
        else:
            self.__read_gdx()

    def __read_gdx(self):
        # Reads the GDX file and extracts a sorted Matrix.
        tstart_total = timer()
        self.logger.info('Reading from GDX started...')
        gdx_h = gdx.new_gdxHandle_tp()

        # initialize GAMS workspace
        err, _ = gdx.gdxCreateD(gdx_h, self.gams_dir, gdx.GMS_SSSIZE)
        if err != 1:
            raise GDXError('Could not initialize GDX workspace - call: gdxCreateD')

        # open gdx file
        err, _ = gdx.gdxOpenRead(gdx_h, self.gdx_path)
        if err != 1:
            raise GDXError('Could not open GDX file - call: gdxOpenRead File=' + str(self.gdx_path))

        # get gdx info
        err, sym_cnt, elem_cnt = gdx.gdxSystemInfo(gdx_h)
        if err != 1:
            raise GDXError('Could not retrieve GDX Info - call: gdxSystemInfo')

        sym_dict = {}
        # iteration over all symbol assigned by their symbol count index (each symbol number)
        for isnr in range(sym_cnt):
            err, name, dim, dtype = gdx.gdxSymbolInfo(gdx_h, isnr)
            if err != 1:
                raise GDXError('Could not retrieve Symbol info - call: gdxSymbolInfo SymbolIdent=' + str(isnr))

            err, rec_cnt, _, cmt = gdx.gdxSymbolInfoX(gdx_h, isnr)
            if err != 1:
                raise GDXError('Could not retrieve Symbol info - call: gdxSymbolInfoX SymbolIdent=' + str(isnr))

            sym_dict[name] = isnr

        err, self._nbVars, _, _ = gdx.gdxSymbolInfoX(gdx_h, sym_dict['j'])
        if err != 1:
            raise GDXError('Could not retrieve Symbol info - call: gdxSymbolInfoX SymbolIdent=j')
        err, self._nbConstr, _, _ = gdx.gdxSymbolInfoX(gdx_h, sym_dict['i'])
        if err != 1:
            raise GDXError('Could not retrieve Symbol info - call: gdxSymbolInfoX SymbolIdent=i')

        tstart_var = timer()
        self.__varNames = np.empty(shape=self._nbVars, dtype='object')
        self.__varStage = np.empty(shape=self._nbVars)
        err, _ = gdx.gdxDataReadRawStart(gdx_h, sym_dict['x'])
        if err != 1:
            raise GDXError('Could not initialize data read - call: SymbolIdent=x')

        txt_reg_ex = re.compile(r'(\S+)')

        var_min = 0
        var_max = 0
        prog_check = math.ceil(self._nbVars / 20)
        for i in range(self._nbVars):
            err, labels, data, _ = gdx.gdxDataReadRaw(gdx_h)
            if err != 1:
                raise GDXError('Could not read record - call: gdxDataReadRaw SymbolIdent=x')

            if i == 0:
                var_min = labels[0]
                var_max = labels[0]

            if labels[0] < var_min:
                var_min = labels[0]
            if labels[0] > var_max:
                var_max = labels[0]

            if self.__plain is True:
                self.__varStage[i] = 1
            else:
                self.__varStage[i] = data[4]

            if not self.__skip_names:
                err, txt, _ = gdx.gdxGetElemText(gdx_h, int(labels[0]))
                if err != 1:
                    raise GDXError('Could not terminate reading operation - call: gdxGetElemText')
                txt_match = txt_reg_ex.search(txt)
                if txt_match is not None:
                    self.__varNames[i] = txt_match.group(1)
            else:
                self.__varNames[i] = "x{}".format(i)

            if i % prog_check == 0:
                self.logger.debug("Variable reading progress: {0:2.0f} % \r".format(100 * i / self._nbVars))

        err = gdx.gdxDataReadDone(gdx_h)
        if err != 1:
            raise GDXError('Could not terminate reading operation - call: gdxDataReadDone SymbolIdent=x')
        tend_var = timer()
        self.logger.info('Finished reading %.i variables in %.2f seconds.' % (self._nbVars, tend_var - tstart_var))

        tstart_equ = timer()
        self.__equNames = np.empty(shape=self._nbConstr, dtype='object')
        self.__equStage = np.empty(shape=self._nbConstr)
        err, _ = gdx.gdxDataReadRawStart(gdx_h, sym_dict['e'])
        if err != 1:
            raise GDXError('Could not initialize data read - call: gdxDataReadRawStart SymbolIdent=e')

        equ_min = 0
        equ_max = 0
        prog_check = math.ceil(self._nbConstr / 20)
        for i in range(self._nbConstr):
            err, labels, data, _ = gdx.gdxDataReadRaw(gdx_h)
            if err != 1:
                raise GDXError('Could not read record - call: gdxDataReadRaw SymbolIdent=e')
            if i == 0:
                equ_min = labels[0]
                equ_max = labels[0]
            if labels[0] < equ_min:
                equ_min = labels[0]
            if labels[0] > equ_max:
                equ_max = labels[0]

            if self.__plain is True:
                self.__equStage[i] = 1
            else:
                self.__equStage[i] = data[4]

            if not self.__skip_names:
                err, txt, _ = gdx.gdxGetElemText(gdx_h, int(labels[0]))
                if err != 1:
                    raise GDXError('Could not terminate reading operation - call: gdxGetElemText')
                txt_match = txt_reg_ex.search(txt)
                if txt_match is not None:
                    self.__equNames[i] = txt_match.group(1)
            else:
                self.__equNames[i] = "e{}".format(i)

            if i % prog_check == 0:
                self.logger.debug("Equation reading progress: {0:2.0f} %\r".format(100 * i / self._nbConstr))

        err = gdx.gdxDataReadDone(gdx_h)
        if err != 1:
            raise GDXError('Could not terminate reading operation - call: gdxDataReadDone SymbolIdent=e')
        tend_equ = timer()
        self.logger.info('Finished reading %.i equations in %.2f seconds.' % (self._nbConstr, tend_equ - tstart_equ))

        # Get order to sort elements by
        equ_sorter = self.__equStage.argsort(kind='mergesort')
        var_sorter = self.__varStage.argsort(kind='mergesort')

        # Sort stages
        self.__equStage = self.__equStage[equ_sorter]
        self.__varStage = self.__varStage[var_sorter]

        # Sort names
        self.__equNames = self.__equNames[equ_sorter]
        self.__varNames = self.__varNames[var_sorter]

        # Get unsorters for generation of A matrix
        self.__varSortUnsort = [(var_sorter[i], i) for i in np.arange(var_sorter.shape[0])]
        self.__varSortUnsort = [(v, i) for i, v in enumerate(var_sorter)]
        self.__varSortUnsort.sort()
        _, self.__varUnsorter = zip(*self.__varSortUnsort)
        self.__varUnsorter = list(self.__varUnsorter)

        self.__equSortUnsort = [(equ_sorter[i], i) for i in np.arange(equ_sorter.shape[0])]
        self.__equSortUnsort.sort()
        _, self.__equUnsorter = zip(*self.__equSortUnsort)
        self.__equUnsorter = list(self.__equUnsorter)

        tstart_mat_a = timer()
        self.__localA = sparse.lil_matrix((equ_max - equ_min + 1, var_max - var_min + 1), dtype='bool')

        err, self._nbNonZero, _, cmt = gdx.gdxSymbolInfoX(gdx_h, sym_dict['A'])
        if err != 1:
            raise GDXError('Could not retrieve Symbol info - call: gdxSymbolInfoX SymbolIdent=A')

        err, _ = gdx.gdxDataReadRawStart(gdx_h, sym_dict['A'])
        if err != 1:
            raise GDXError('Could not initialize data read - call: gdxDataReadRawStart SymbolIdent=A')

        prog_check = math.ceil(self._nbNonZero / 20)
        for i in range(self._nbNonZero):
            err, labels, data, _ = gdx.gdxDataReadRaw(gdx_h)
            if err != 1:
                raise GDXError('Could not read record - call: gdxDataReadRaw SymbolIdent=A')

            # Element data contains the matrix coefficients, since only nonzero entries are marked this is not relevant
            self.__localA[self.__equUnsorter[labels[0] - equ_min], self.__varUnsorter[labels[1] - var_min]] = 1
            # data[0] contains the coefficients, they are not needed for the matrix plotting
            if i % prog_check == 0:
                self.logger.debug("Matrix reading progress: {0:2.0f} %\r".format(100 * i / self._nbNonZero))

        err = gdx.gdxDataReadDone(gdx_h)
        if err != 1:
            raise GDXError('Could not terminate reading operation - call: gdxDataReadDone SymbolIdent=A')

        tend_mat_a = timer()
        self.logger.info('Finished reading matrix with %.i non-zero elements in %.2f seconds.' % (self._nbNonZero,
                                                                                                  tend_mat_a - tstart_mat_a))
        tend_total = timer()
        self.logger.info('Reading from GDX finished after {0:.2f} seconds.'.format(tend_total - tstart_total))

        if self.__save_numpy:
            self.__write_numpy()

    def __write_numpy(self):
        # Save the information from gdx to numpy binary data
        self.logger.info('Saving gdx information to {}.'.format(self.npz_path))
        np.savez(self.npz_path, matrixA_data=self.__localA.tocsr().data,
                 matrixA_indices=self.__localA.tocsr().indices,
                 matrixA_indptr=self.__localA.tocsr().indptr, matrixA_shape=self.__localA.tocsr().shape,
                 varStage_data=self.__varStage.data, varNames_data=self.__varNames.data,
                 equStage_data=self.__equStage.data, equNames_data=self.__equNames.data)

    def __read_numpy(self):
        tstart_load = timer()
        loader = np.load(self.npz_path)
        self.logger.info('Reading information from {}.'.format(self.npz_path))
        self.__localA = sparse.csr_matrix(
            (loader['matrixA_data'], loader['matrixA_indices'], loader['matrixA_indptr']),
            shape=loader['matrixA_shape'])
        self.__varStage = loader['varStage_data']
        self.__varNames = loader['varNames_data']
        self.__equStage = loader['equStage_data']
        self.__equNames = loader['equNames_data']

        tend_load = timer()
        self.logger.info('Finished reading information in {0:.2f} seconds.'.format(tend_load - tstart_load))

    def _get_block_stats(self):
        self.logger.debug('Started method \'_get_block_stats\'')
        self._nbVars = self._sparseA.shape[1]
        self._nbConstr = self._sparseA.shape[0]
        self._nbNonZero = self._sparseA.getnnz()

        self._varStages = np.unique(self._varStage)
        self._equStages = np.unique(self._equStage)

        # Here we have several different cases that can occur:
        # - full jacobian with 1:n varStages and 1:n+1 equStages:
        # - partial jacobian with varStages 1,k and equStages 1,k,n

        # Check if numbering of blocks is consecutive if full jacobian is provided
        if len(self.varStages) > 2 and len(self.equStages) > 3:
            if np.max(np.diff(self.equStages)) > 1 or np.max(np.diff(self.equStages)) > 1:
                self.logger.warning("Blocks need to be numbered consecutively. Stages {} found in the gdx file.".format(
                    np.unique(np.union1d(self.equStages, self.varStages))))

        # Check if block 1 is contained in the linking variable stages
        if 1 in self.varStages:
            self._linkVarStage = [1.0]
        else:
            self._linkVarStage = []

        # Check if block 1 is contained in the linking equation stages
        self._linkVarStage = [np.min(self.varStages)]
        if 1 in self.equStages and np.max(self.equStages) > 1:
            self._linkEquStages = [1.0]
        else:
            self._linkEquStages = []

        # Check if n+2 block is only in equation stages, otherwise it is a stochastical model
        if np.max(self.equStages) > np.max(self.varStages):
            self._linkEquStages.append(np.max(self.equStages))

        self.logger.debug('Calculating and counting block stages')
        # Calculate block stages and count elements in the variable and equation arrays
        self._blockStages = np.setdiff1d(np.union1d(self.equStages, self.varStages),
                                         np.union1d(self.linkEquStages, self.linkVarStage))
        self._nbBlocks = len(self.blockStages)

        self._nbLinkVars = np.count_nonzero(np.equal(self.__varStage, self.linkVarStage))

        if 1 in self.linkEquStages:
            self._nbLinkConsA0 = np.count_nonzero(np.equal(self.__equStage, 1))
        else:
            self._nbLinkConsA0 = 0

        if np.max(self.linkEquStages) > 1:
            self._nbLinkConsAn = np.count_nonzero(np.equal(self.__equStage, np.max(self.linkEquStages)))
        else:
            self._nbLinkConsAn = 0

        self._nbLinkCons = self.nbLinkConsA0 + self.nbLinkConsAn

    def _get_local_constr(self, sparse_entries=4):
        self.logger.debug('Started method \'_get_local_constr\'')
        tstart_get_local_constr = timer()

        self._nbLinkConsAn_local = 0

        if np.max(self.linkEquStages) > 1:
            bool_equ_stage = np.in1d(self._equStage, np.max(self.linkEquStages))
            sparseA_link_constr = self._sparseA[bool_equ_stage, :]

            prog_check = math.ceil(sparseA_link_constr.shape[0] / 20)

            self.logger.debug('Checking {} constraints'.format(sparseA_link_constr.shape[0]))
            for irow in range(sparseA_link_constr.shape[0]):

                # Check only sequential block stages in range
                row_link_constr = np.array(sparseA_link_constr.getrow(irow).todense())[0]
                row_An_stage = self._varStage[row_link_constr]
                if row_An_stage[-1]-row_An_stage[0] < sparse_entries and row_An_stage[0] > 1:
                    self._nbLinkConsAn_local = self._nbLinkConsAn_local + 1

                if irow % prog_check == 0:
                    self.logger.debug(
                        "Sparse constraint checking progress: {0:2.0f} %\r".format(
                            100 * irow / sparseA_link_constr.shape[0]))

        tend_get_local_constr = timer()
        self.logger.info('Finished checking sparse constraints in {0:.2f} seconds.'.format(
            tend_get_local_constr - tstart_get_local_constr))

    def _get_local_vars(self, sparse_entries=4):
        self.logger.debug('Started method \'_get_local_vars\'')
        tstart_get_local_vars = timer()

        self._nbLinkVars_local = 0

        bool_var_stage = np.in1d(self._varStage, [1])
        sparseA_link_vars = self._sparseA.tocsc()[:, bool_var_stage]

        prog_check = math.ceil(sparseA_link_vars.shape[1] / 20)

        self.logger.debug('Checking {} variables'.format(sparseA_link_vars.shape[1]))
        for icol in range(sparseA_link_vars.shape[1]):
            col_link_vars = sparseA_link_vars.getcol(icol)

            # Check sparse constraints with up to two entries
            if col_link_vars.count_nonzero() <= sparse_entries:
                self._nbLinkVars_local = self._nbLinkVars_local + 1

            if icol % prog_check == 0:
                self.logger.debug(
                    "Sparse variable checking progress: {0:2.0f} %\r".format(100 * icol / sparseA_link_vars.shape[1]))

        tend_get_local_vars = timer()
        self.logger.info('Finished checking sparse variables in {0:.2f} seconds.'.format(
            tend_get_local_vars - tstart_get_local_vars))

    def __get_dense_cols(self):
        self.logger.debug('Started method \'__get_dense_cols\'')
        if self.__colNNZ is None:
            self.__colNNZ = np.empty(shape=self._nbVars)
            prog_check = math.ceil(self.__localA.get_shape()[1] / 20)

            for icol in range(self.__localA.get_shape()[1]):
                count_nnz = self.__localA.getcol(icol).getnnz()
                self.__colNNZ[icol] = count_nnz
                if icol % prog_check == 0:
                    self.logger.info("Variable checking progress: {0:2.0f} %\r".format(100 * icol /
                                                                                       self.__localA.get_shape()[1]))

            self.__denseSorted = np.flipud(self.__colNNZ.argsort())
        return self.__colNNZ, self.__denseSorted

    def check_annot(self, list_errors=False, check_A0=False):
        self.logger.debug('Started method \'check_annot\'')

        if len(self.varStages) <= 2 and len(self.equStages) <= 3:
            self.logger.warning("Annotation checking does not work for plain matrices and partial jacobian")
            return

        if len(self.varStages) > len(self.equStages):
            self.logger.error("More variable stages then equations stages found")
            return

        tstart_check_ann = timer()

        # First we check the columns
        # Case 1: Linking Variables should be used in a block constraint stage or a An linking constraint stage
        # Case 2: In block variable stages we need to check if entries are

        if check_A0:
            error_link_var_list = []
            bool_var_stage = np.in1d(self._varStage, [1])
            sparseA_link_vars = self._sparseA.tocsc()[:, bool_var_stage]

            prog_check = math.ceil(sparseA_link_vars.shape[1] / 20)

            self.logger.debug('Checking {} linking variables'.format(sparseA_link_vars.shape[1]))
            for icol in range(sparseA_link_vars.shape[1]):

                col_stages = np.unique(self._equStage[np.array(sparseA_link_vars.getcol(icol).todense())[:, 0]])
                if len(col_stages[~np.in1d(col_stages, [1])]) == 0:
                    error_link_var_list.append(self._varNames[icol])

                if icol % prog_check == 0:
                    self.logger.debug(
                        "Linking variables checking progress: {0:2.0f} %\r".format(
                            100 * icol / sparseA_link_vars.shape[1]))

        # Then we check the block constraints
        error_block_equ_list = []
        bool_equ_stage = np.in1d(self._equStage, np.unique(self.varStages))

        self.logger.debug('Checking {} constraints with stage {}'.format(len(bool_equ_stage), self.varStages))
        cnt = 0
        for irow, icheck in zip(np.arange(self._sparseA.get_shape()[0]), bool_equ_stage):
            if icheck:
                if cnt < 10:
                    cnt = cnt+1
                    self.logger.debug('Row: {} - Name: {} - Stage: {}'.format(irow, self._equNames[irow], self._equStage[irow]))

                row_stages = np.unique(self._varStage[np.array(self._sparseA.getrow(irow).todense())[0]])
                if len(row_stages[~np.in1d(row_stages, [1, self._equStage[irow]])]) > 0:
                    error_block_equ_list.append([self._equNames[irow],
                                                 np.unique(row_stages[~np.in1d(row_stages, [1, self._equStage[irow]])]),
                        [1, self._equStage[irow]]
                    ])

        # Then we check the linking constraints
        error_link_equ_list = []

        if np.max(self.linkEquStages) > 1:
            bool_equ_stage = np.in1d(self._equStage, np.max(self.linkEquStages))

            self.logger.debug('Checking {} linking constraints with stage {}'.format(len(bool_equ_stage),
                                                                                     np.max(self.linkEquStages)))

            cnt = 0
            for irow, icheck in zip(np.arange(self._sparseA.get_shape()[0]), bool_equ_stage):
                if icheck:
                    if cnt < 10:
                        cnt = cnt + 1
                        self.logger.debug(
                            'Row: {} - Name: {} - Stage: {}'.format(irow, self._equNames[irow], self._equStage[irow]))

                    row_stages = np.unique(self._varStage[np.array(self._sparseA.getrow(irow).todense())[0]])
                    if len(row_stages[~np.in1d(row_stages, [1])]) == 0:
                        error_link_equ_list.append(self._equNames[irow])

        tend_checkAnn = timer()
        self.logger.info('Finished checking {0:.0f} equations in {1:.2f} seconds.'.format(self.__localA.get_shape()[0],
                                                                                          tend_checkAnn - tstart_check_ann))

        if check_A0:
            if len(error_link_var_list) == 0:
                self.logger.info('All linking variables occur in A1 to An')
            else:
                self.logger.warning(
                    "Variable stage error encountered in {} block equations.".format(len(error_link_var_list)))
                if list_errors:
                    for err_name in error_link_var_list:
                        self.logger.warning('Linking variable {} occures only in block A0'.format(err_name))

        if len(error_block_equ_list) == 0:
            self.logger.info("All block equations have only entries in correct variables stages.")
        else:
            self.logger.warning(
                "Variable stage error encountered in {} block equations.".format(len(error_block_equ_list)))
            if list_errors:
                for err_name, err_found, err_exp in error_block_equ_list:
                    self.logger.warning(
                        "Block equation {} has entries in variable stages {} while only stages {} were expected.".format(
                            err_name, err_found, err_exp))

        if len(error_link_equ_list) == 0:
            self.logger.info("All linking equations have entries block in variables assigned.")
        else:
            self.logger.warning("{} linking equations have no entries in block variable stages.".format(
                len(error_link_equ_list)))
            if list_errors:
                for err_name in error_link_equ_list:
                    self.logger.warning("Linking equation {} has no entries in block variable stages.".format(err_name))

    def plot(self, truncLabelIndex=False, colormap=__colormap, greyscale=__greyscale, stage=None):
        # Recompile equation and variable names (cutting off indexes)
        if truncLabelIndex:
            self.__truncLabels()

        self.bool_var_stage = np.full(self._varStage.shape, True, dtype=bool)
        self.bool_equ_stage = np.full(self._equStage.shape, True, dtype=bool)

        if stage is not None:
            self.bool_var_stage = np.in1d(self._varStage, [self._equStage.min(), stage])
            self.bool_equ_stage = np.in1d(self._equStage, [self._equStage.min(), stage, self._equStage.max()])

        if self.__plain is True:
            stagePatches = []
        else:
            stagePatches = self.__createPatches(colormap=colormap, greyscale=greyscale)

        # Plotting
        self.logger.info('Plotting started...')
        fig = plt.figure()
        axes = fig.add_subplot(111, aspect='equal')

        if len(stagePatches) > 0:
            for p in stagePatches:
                axes.add_patch(p)

        if stage is None:
            axes.spy(self.__localA, markersize=1, precision=0, c='k')
        else:
            axes.spy(self.__localA[self.bool_equ_stage, :][:, self.bool_var_stage], markersize=1, precision=0, c='k')

        def getEquName(y, pos):
            if y <= len(self.__equNames):
                return self.__equNames[int(y)]
            return

        def getVarName(x, pos):
            if x <= len(self.__varNames):
                return self.__varNames[int(x)]
            return

        axes.xaxis.set_major_formatter(ticker.FuncFormatter(getVarName))
        axes.yaxis.set_major_formatter(ticker.FuncFormatter(getEquName))

        plt.xticks(rotation=45, horizontalalignment='left')
        axes.xaxis.set_major_locator(ticker.MaxNLocator(nbins=20, integer='true'))
        axes.yaxis.set_major_locator(ticker.MaxNLocator(nbins=20, integer='true'))

        figManager = plt.get_current_fig_manager()
        figManager.window.showMaximized()
        plt.show()

    def create_png(self, width=10, dpi=150, markersize=1, colormap=__colormap, greyscale=__greyscale, stage=None,
                   filename=None):

        self.bool_var_stage = np.full(self._varStage.shape, True, dtype=bool)
        self.bool_equ_stage = np.full(self._equStage.shape, True, dtype=bool)

        if stage is not None:
            self.bool_var_stage = np.in1d(self._varStage, [self._equStage.min(), stage])
            self.bool_equ_stage = np.in1d(self._equStage, [self._equStage.min(), stage, self._equStage.max()])

        if self.__plain is True:
            stagePatches = []
        else:
            stagePatches = self.__createPatches(colormap=colormap, greyscale=greyscale)

        width = width * 2.54

        # Printing
        self.logger.info('Creating png...')
        fig = plt.figure(num=None, figsize=(width, width / len(self._varStage[self.bool_var_stage])
                                            * len(self._equStage[self.bool_equ_stage])),
                         dpi=dpi, facecolor='w', edgecolor='k')

        axes = fig.add_subplot(111, aspect='equal', position=[0., 0., 1., 1.])
        axes.axis('off')

        if len(stagePatches) > 0:
            for p in stagePatches:
                axes.add_patch(p)

        if stage is None:
            axes.spy(self.__localA, markersize=markersize, precision=0, c='k')
        else:
            A_plot = self.__localA[self.bool_equ_stage, :]
            A_plot = A_plot.tocsc()
            A_plot = A_plot[:, self.bool_var_stage]
            axes.spy(A_plot, markersize=1, precision=0, c='k')

        if filename is None:
            filename = self.__file_name

        png_file = 'plots/' + filename + '.png'
        if stage is not None:
            png_file = 'plots/' + filename + '_' + str(int(stage)) + '.png'

        fig.savefig(png_file)
        self.logger.info('Plot saved as ' + png_file)

    def __truncLabels(self):
        tstart_indexRecomp = timer()
        txtRegEx = re.compile('(\w+)')
        for i in range(np.count_nonzero(self.__varNames)):
            txtMatch = txtRegEx.search(str(self.__varNames[i]))
            if txtMatch is not None:
                self.__varNames[i] = txtMatch.group(1)
        for i in range(np.count_nonzero(self.__equNames)):
            txtMatch = txtRegEx.search(str(self.__equNames[i]))
            if txtMatch is not None:
                self.__equNames[i] = txtMatch.group(1)
        tend_indexRecomp = timer()
        self.logger.info('Finished recompiling names in %.2f seconds.' % (tend_indexRecomp - tstart_indexRecomp))

    def __createPatches(self, colormap, greyscale):
        stagePatches = []

        if greyscale:
            self.logger.warning("Grey scale option not implemented yet")

        if self.nbBlocks > 0:
            # Get colormap for blocks
            cmap = colormap
            if type(colormap) is not colors.LinearSegmentedColormap:
                cmap = plt.cm.get_cmap(colormap)

            # Automatically generate blocks based on block stages
            for stage in self.blockStages:
                equ_first = np.argmax(np.equal(self.__equStage[self.bool_equ_stage], stage))
                equ_count = np.count_nonzero(np.equal(self.__equStage[self.bool_equ_stage], stage))
                var_first = np.argmax(np.equal(self.__varStage[self.bool_var_stage], stage))
                var_count = np.count_nonzero(np.equal(self.__varStage[self.bool_var_stage], stage))
                stagePatches.append(patches.Rectangle((var_first, equ_first), var_count, equ_count,
                                                      color=cmap((stage - 1) / (np.max(self.blockStages) - 1))))

        # Mark equations assigned to stage n+2
        if np.max(self.linkEquStages) > 1:
            stagePatches.append(
                patches.Rectangle((np.count_nonzero(self.__varStage[self.bool_var_stage]),
                                   np.count_nonzero(self.__equStage[self.bool_equ_stage])),
                                  -np.count_nonzero(self.__varStage[self.bool_var_stage]),
                                  -np.count_nonzero(np.equal(self.__equStage, np.max(self.linkEquStages))),
                                  color=(0.7, 0.7, 0.7, 1)))

        # Mark variables assigned to stage 1
        if np.max(self.linkVarStage) == 1:
            stagePatches.append(patches.Rectangle((0, 0),
                                                  np.count_nonzero(np.equal(self.__varStage[self.bool_var_stage],
                                                                    self.linkVarStage)),
                                                  np.count_nonzero(self.__equStage[self.bool_equ_stage]),
                                                  color=(0.7, 0.7, 0.7, 1)))

        return stagePatches
