# -*- coding:utf-8 -*-
from __future__ import print_function

import os
import sys

# Set GAMS path and add the path to the python sys to correctly load gdxcc
gamsDir = os.path.abspath('C:/GAMS/win64/27.1')
sys.path.append(os.path.join(gamsDir, 'apifiles', 'Python', 'api_36'))
from annotation_plotting import oAnnotGDX

if __name__ == '__main__':
    filePath = os.path.abspath('C:/Work/REMix-OaM/OptiMo/projects/prj_UNSEEN/YSSP_mip/data/sus_Base/sus_Exp/sus_DC-trans/sus_NoTaW/OptiMoSystem_jacobian.gdx')
    gdx = oAnnotGDX(filePath, gamsDir)

    gdx.check_annot()  # Check for annotation errors
    gdx.plot()         # Plot the annotation
